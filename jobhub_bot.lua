
--<hi
--<hello
--<hey
function application_field_greet()
  reply("Hi, I'm Jobhub.").send()
  reply()
      .image("https://media.giphy.com/media/Mp4hQy51LjY6A/giphy.gif")
      .send()
  engage("jobhub_main_flow")
end

-->What's your name?
--<[_PERSON_]
function application_field_name()
  reply("Nice to meet you, {person(0).name}").send()
  fulfil(person(0).name)
end

-->
function application_field_user()
  reply("Are you a recruiter or a job seeker?")
      .action()
          .title("A recruiter")
      .action()
          .title("A job seeker")
      .send()
end

--<A recruiter
function application_field_user_recruiter()
  fulfil("recruiter")
end

--<A job seeker
function application_field_user_jobseeker()
  fulfil("job_seeker")
end


------------------------------------------------------------------------------------------------------------------------------------
--                                                   COLLECT INFO
------------------------------------------------------------------------------------------------------------------------------------

-->What's your company name?
--<[*]
function application_field_company()
  fulfil(message.text)
end

-->
function application_field_industry()
  local question = ""
  if context.user.value == "recruiter" then
      question = "Which industry do you want to hire?"
  elseif context.user.value == "job_seeker" then
      question = "Which industry do you want to apply?"
  end
  reply(question)
      .action()
          .title("IT")
      .action()
          .title("Insurance")
      .action()
          .title("Banking")
      .send()
end

--<IT
function application_field_industry_it()
  fulfil("IT")
end

--<Insurance
function application_field_industry_insurance()
  fulfil("Insurance")
end

--<Banking
function application_field_industry_banking()
  fulfil("Banking")
end

-->
function application_field_role()
  local question = ""
  if context.user.value == "recruiter" then
      question = "Which role do you want to hire?"
  elseif context.user.value == "job_seeker" then
      question = "Which role do you want to apply?"
  end
  if context.industry.value == "IT" then
      reply(question)
          .action()
              .title("Frontend Developer")
          .action()
              .title("Backend Developer")
          .send()
  elseif context.industry.value == "Banking" then
      reply(question)
          .action()
              .title("Sales Coordinator")
          .action()
              .title("Business Analyst")
          .send()
  elseif context.industry.value == "Insurance" then
      reply(question)
          .action()
              .title("Accountant")
          .action()
              .title("Financial Analyst")
          .send()
  end
end

--<Frontend Developer
function application_field_role_it1()
  fulfil("Frontend Developer")
end

--<Backend Developer
function application_field_role_it2()
  fulfil("Backend Developer")
end

--<Sales Coordinator
function application_field_role_banking1()
  fulfil("Sales Coordinator")
end

--<Business Analyst
function application_field_role_banking2()
  fulfil("Business Analyst")
end

--<Accountant
function application_field_role_insurance1()
  fulfil("Accountant")
end

--<Financial Analyst
function application_field_role_insurance2()
  fulfil("Financial Analyst")
end

-->
function application_field_location()
  local question = ""
  if context.user.value == "recruiter" then
      question = "Where do you want to hire?"
  elseif context.user.value == "job_seeker" then
      question = "Where do you want to work?"
  end
  reply(question)
      .action()
          .title("KL")
      .action()
          .title("Penang")
      .action()
          .title("Selangor")
      .send()
end

--<KL
function application_field_location_kl()
  fulfil("KL")
end

--<Penang
function application_field_location_penang()
  fulfil("Penang")
end

--<Selangor
function application_field_loction_selangor()
  fulfil("Selangor")
end

-->
function application_field_experience()
  if context.user.value == "recruiter" then
      reply("How many years of expected experience is the candidate?").send()
  elseif context.user.value == "job_seeker" then
      reply("How many years of experience do you have as {context.role.value}?").send()
  end
  application_field_getExperience()
end

--<[_NUMBER_]
function application_field_getExperience()
  fulfil(number(0).n)
end

-->What's your budget in RM?
--<[_MONEY_]
function application_field_budget()
  fulfil(money(0).n)
end

-->How would you describe your job?
--<[*]
function application_field_description()
  fulfil(message.text)
end

-->What's your expected salary in RM?
--<[_MONEY_]
function application_field_expected_salary()
  fulfil(money(0).n)
end

-->What's your email address?
--<[_EMAIL_]
function application_field_email()
  fulfil(email(0).address)
end

-->
function application_field_confirmation()
  if context.user.value == "recruiter" then
      local jobDetail = 
          F"Your company name is {context.company.value}. It provides products and services in {context.industry.value} industry. "..
          F"You want to hire a {context.role.value} in {context.location.value}. "..
          F"This role requires at least {context.experience.value} years of experience. "..
          F"You can pay upto RM{context.budget.value}. "..
          F"Candidates can contact you via the email address {context.email.value}. "..
          F"And your job description is: {context.description.value} "
      reply("Here is the detail of your job post!").send()
      reply(jobDetail).send()
  elseif context.user.value == "job_seeker" then
      local candidateDetail =
          F"You have {context.experience.value} years working in {context.industry.value} industry as a {context.role.value}. "..
          F"You are looking for a job in {context.location.value}. "..
          F"You expect to be paid RM{context.expected_salary.value} each month. "..
          F"Recruiters can reach you via the email address {context.email.value}."
      reply("Here is your details").send()
      reply(candidateDetail).send()
  end
  reply("Is everything correct?")
      .action()
          .title("Yes")
      .action()
          .title("No")
      .send()
end

--<Yes
function application_field_confirmation_yes()
  local tiles = {}
  local tileCount = 0
  if context.user.value == "recruiter" then
      tiles = reply("List of potential candidates that match your job requirements, yayyy! 😘")
      for _, candidate in pairs(CANDIDATES) do
          if isMatchedCandidate(candidate) then
              tiles
                  .tile(candidate.name)
                      .text(F"A {candidate.role} with {candidate.experience} years of working experience.")
                      .image("https://image.flaticon.com/icons/svg/1412/1412230.svg")
                      .action()
                          .title("Send an email")
                          .url("mailto:sf@gma.com")
              tileCount = tileCount + 1
          end
      end
      if tileCount < 1 then 
          tiles = reply("I'm sorry, I can't find any candidates that matches your requirements. 😔")
                      .action()
                          .title("Change requirements")
                      .action()
                          .title("Quit")
      end
  elseif context.user.value == "job_seeker" then
      tiles = reply("List of your dream jobs, yayyy! 😀")
      for _, job in pairs(JOBS) do
          if isMatchedJob(job) then
              tiles
                  .tile(job.role .. " at " .. job.company)
                      .text(job.description)
                      .image("https://image.flaticon.com/icons/svg/1412/1412191.svg")
                      .action()
                          .title("Apply")
                          .url("mailto:test@gmail.com")
              tileCount = tileCount + 1
          end
      end
      if tileCount < 1 then 
          tiles = reply("I'm sorry, I can't find any jobs that matches your profile. 😔")
                      .action()
                          .title("Update profile")
                      .action()
                          .title("Quit")
      end
  end
  tiles.send()
  fulfil("Yes")
end

--<No
--<Change requirements
--<Update profile
function application_field_confirmation_no()
  reply("Which field do you want to change?").send()
  if context.user.value == "recruiter" then
      reply()
          .action()
              .title("Company")
          .action()
              .title("Industry")
          .action()
              .title("Role")
          .action()
              .title("Location")
          .action()
              .title("Experience")
          .action()
              .title("Budget")
          .action()
              .title("Description")
          .action()
              .title("Email")
          .send()
  elseif context.user.value == "job_seeker" then
      reply()
          .action()
              .title("Industry")
          .action()
              .title("Role")
          .action()
              .title("Location")
          .action()
              .title("Experience")
          .action()
              .title("Expected salary")
          .action()
              .title("Email")
          .send()
  end
end

--<Company
function application_field_confirmation_company()
  context.company = nil
  application_field_company()
end

--<Industry
function application_field_confirmation_industry()
  context.industry = nil
  application_field_industry()
end

--<Role
function application_field_confirmation_role()
  context.role = nil
  application_field_role()
end

--<Location
function application_field_confirmation_location()
  context.location = nil
  application_field_location()
end

--<Experience
function application_field_confirmation_experience()
  context.experience = nil
  application_field_experience()
end

--<Description
function application_field_confirmation_description()
  context.description = nil
  application_field_description()
end

--<Budget
function application_field_confirmation_budget()
  context.budget = nil
  application_field_budget()
end

--<Expected salary
function application_field_confirmation_expected_salary()
  context.expected_salary = nil
  application_field_expected_salary()
end

--<Email
function application_field_confirmation_email()
  context.email = nil
  application_field_email()
end


------------------------------------------------------------------------------------------------------------------------------------
--                                                   Goodbye
------------------------------------------------------------------------------------------------------------------------------------

-->
function application_field_goodbye()
  reply("Do you want to continue?")
      .action()
          .title("Yes, let's continue.")
      .action()
          .title("No, I want to quit.")
      .send()
end

--<Yes, let's continue.
function application_field_goodbye_continue()
  if context.user.value == "recruiter" then
      resetJobInfo()
      application_field_company()
  elseif context.user.value == "job_seeker" then
      resetProfile()
      application_field_industry()
  end
end

--<No, I want to quit.
--<Quit
function application_field_goodbye_quit()
  reply("Thanks and see you soon! 😉").send()
end
