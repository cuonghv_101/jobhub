
--<got it
--<cool
--<great
--<awesome
--<nice
function application_field_great()
  local response = choice({
      "That's great to know!",
      "😀",
      "👌"
  })
  reply(response).send()
end
