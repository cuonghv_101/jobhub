
function isMatchedCandidate(candidate)
  return candidate.industry == context.industry.value 
          and candidate.role == context.role.value 
          and candidate.location == context.location.value
          and candidate.experience >= context.experience.value
          and candidate.expected_salary <= context.budget.value
end

function isMatchedJob(job)
  return job.industry == context.industry.value 
          and job.role == context.role.value 
          and job.location == context.location.value
          and job.experience <= context.experience.value
          and job.budget >= context.expected_salary.value
end

function resetJobInfo()
  context.company = nil
  context.industry = nil
  context.role = nil
  context.location = nil
  context.experience = nil
  context.budget = nil
  context.description = nil
  context.email = nil
  context.confirmation = nil
end

function resetProfile()
  context.industry = nil
  context.role = nil
  context.location = nil
  context.experience = nil
  context.expected_salary = nil
  context.email = nil
  context.confirmation = nil
end

function manualFulfil(value, key)
  context[key] = {
      value= value,
      updatedAt= time().unix,
  }
end
