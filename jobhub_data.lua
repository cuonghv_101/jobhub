
-- JOBHUB JOBS
JOBS = JOBS or {}

JOBS["2001014800086"] = {
    id="2001014800086",
    company="Jirnexu",
    industry="IT",
    role="Frontend Developer",
    location="KL",
    experience=2,
    budget=6000,
    description="This job requires HTML, CSS, JS skills",
    email="recruiter1@st.com"
}

JOBS["2001014800087"] = {
    id="2001014800087",
    company="Amazon",
    industry="IT",
    role="Backend Developer",
    location="Selangor",
    experience=4,
    budget=6000,
    description="This job requires Golang, PHP skills",
    email="recruiter1@st.com"
}

JOBS["2001014800078"] = {
    id="2001014800078",
    company="CIMB",
    industry="Banking",
    role="Sales Coordinator",
    location="KL",
    experience=3,
    budget=6000,
    description="Prepare and confidently deliver presentations of company's DFaaS offering to prospective clients, deliver product demonstrations to convince both clients and distribution/implementation partners",
    email="recruiter1@st.com"
}

JOBS["2001014800074"] = {
    id="2001014800074",
    company="Maybank",
    industry="Banking",
    role="Sales Coordinator",
    location="KL",
    experience=3,
    budget=6000,
    description="This is a front-line sales position in , Direct Sales Force.",
    email="recruiter1@st.com"
}

JOBS["2001014800077"] = {
    id="2001014800077",
    company="HSBC",
    industry="Banking",
    role="Business Analyst",
    location="KL",
    experience=3,
    budget=6000,
    description="Successfully deliver multiple data projects in an agile manner. Achieve Data Analytics project objectives e.g. delivery dates, budget target and data quality goals.",
    email="recruiter1@st.com"
}

JOBS["2001014800066"] = {
    id="2001014800066",
    company="AIA",
    industry="Insurance",
    role="Financial Analyst",
    location="KL",
    experience=2,
    budget=6000,
    description="Works together with the regional bid team during the pursuit process. Develops the financial case for, outsourcing/managed services opportunities within the Company",
    email="recruiter1@st.com"
}

JOBS["2001014800067"] = {
    id="2001014800067",
    company="Etiqa",
    industry="Insurance",
    role="Accountant",
    location="KL",
    experience=2,
    budget=6000,
    description="Supervise / Prepare Payment Claims monthly to be submitted on the due dates given by the Main-Con/Owner",
    email="recruiter1@st.com"
}


-- JOBHUB CANDIDATES
CANDIDATES = CANDIDATES or {}

CANDIDATES["3910125800012"] = {
    id="3910125800012",
    name="Bob",
    industry="IT",
    role="Frontend Developer",
    location="KL",
    experience=2,
    expected_salary=4000,
    email="can1@gmail.com"
}

CANDIDATES["3910125800013"] = {
    id="3910125800002",
    name="Alice",
    industry="IT",
    role="Frontend Developer",
    location="KL",
    experience=4,
    expected_salary=4000,
    email="can1@gmail.com"
}

CANDIDATES["3910125800022"] = {
    id="3910125800022",
    name="Kim",
    industry="Banking",
    role="Business Analyst",
    location="KL",
    experience=6,
    expected_salary=4000,
    email="can1@gmail.com"
}

CANDIDATES["3910125800002"] = {
    id="3910125800002",
    name="Jack",
    industry="Insurance",
    role="Financial Analyst",
    location="KL",
    experience=4,
    expected_salary=4000,
    email="can1@gmail.com"
}

CANDIDATES["3910125800003"] = {
    id="3910125800003",
    name="Klara",
    industry="Insurance",
    role="Financial Analyst",
    location="KL",
    experience=2,
    expected_salary=4000,
    email="can1@gmail.com"
}
