
-- Main flow
jobhub_main_flow = flow({
  id = "jobhub_main_flow",
  context = context,
  intentNameFunc = function(id, name)
          return "application_field_"..name
      end
})
  .question("name")
  .question("user")
      .when("recruiter")
          .detour("jobhub_recruiter_flow")
          .merge()
      .when("job_seeker")
          .detour("jobhub_job_seeker_flow")
          .merge()


-- Recruiter flow
jobhub_recruiter_flow = flow({
  id = "jobhub_recruiter_flow",
  context = context,
  intentNameFunc = function(id, name)
          return "application_field_"..name
      end
})
  .question("company")
  .question("industry")
  .question("role")
  .question("location")
  .question("experience")
  .question("budget")
  .question("description")
  .question("email")
  .question("confirmation")
  .question("goodbye")

-- Job seeker flow
jobhub_job_seeker_flow = flow({
  id = "jobhub_job_seeker_flow",
  context = context,
  intentNameFunc = function(id, name)
          return "application_field_"..name
      end
})
  .question("industry")
  .question("role")
  .question("location")
  .question("experience")
  .question("expected_salary")
  .question("email")
  .question("confirmation")
  .question("goodbye")
